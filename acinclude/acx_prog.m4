dnl ================================================================
dnl PROG_PROG
dnl ================================================================
AC_DEFUN([ACX_PROG_PROG], [
    AC_PATH_PROG([$1],[$2])
    AC_ARG_VAR([$1],[$2 command])
    have_$2=yes
    test "x$$1" = x && $1="false ...$2 program required..." && have_$2=no
])
dnl AC_DEFUN([PROG_DOT], [ACX_PROG_PROG([DOT],[dot])])
